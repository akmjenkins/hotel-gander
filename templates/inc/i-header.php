<?php

	//you can remove these, I only included it so I could simulate WordPress conditionals while I was building the template
	function is_home() {
		global $bodyclass;
		return preg_match('/home/',$bodyclass);
	}

	function is_404() {
		global $bodyclass;
		return preg_match('/error404/',$bodyclass);
	}
?>
<!doctype html>
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]--> 

	<head>
		<title>Hotel Gander</title>
		<meta charset="utf-8">
		
		<?php include('api-keys.php'); ?>
		
		<!-- datepicker css -->
		<link rel="stylesheet" href="../assets/bin/js/modules/datepicker/datepicker.css">

		<!-- modernizr -->
		<script src="../assets/bin/js/lib/modernizr/modernizr.js"></script>
		
		<!-- font awesome -->
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		
		<!-- lato -->
		<link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,700,300italic,400italic,700italic&subset=latin' rel="stylesheet">
		
		<!-- jQuery -->
		<!--[if IE 8 ]>
			<script src="../assets/bin/lib/js/jquery-legacy/jquery.min.js"></script>	
		<![endif]--> 
		<!--[if (gte IE 9)|!(IE)]><!-->
			<script src="../assets/bin/lib/js/jquery/jquery.min.js"></script>	
		<!--<![endif]--> 
		
		<!-- magnific popup -->
		<link rel="stylesheet" href="../assets/bin/lib/css/magnific-popup/magnific-popup.css?<?php echo time(); ?>">
		<script src="../assets/bin/lib/js/magnific-popup/jquery.magnific-popup.min.js?<?php echo time(); ?>"></script>
		
		<!-- favicons -->
		<link rel="icon" type="image/x-icon"  href="../assets/bin/images/favicons/favicon.ico">
		<link rel="icon" type="image/png"  href="../assets/bin/images/favicons/favicon-32.png">
		<link rel="icon" href="../assets/bin/images/favicons/favicon-32.png" sizes="32x32">
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="../assets/bin/images/favicons/favicon-152.png">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/bin/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="../assets/bin/images/favicons/favicon-120.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/bin/images/favicons/favicon-114.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/bin/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" href="../assets/bin/images/favicons/favicon-114.png">	
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="../assets/bin/images/favicons/favicon-144.png">
		
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0">
		
		<!-- place our CSS after all the other stylesheets so that we can more easily override their styles -->
		<link rel="stylesheet" href="../assets/bin/css/style.css?<?php echo time(); ?>">		
		
	</head>
	<body class="<?php echo $bodyclass; ?>">
		<button id="mobile-nav" class="transform-button fa fa-abs fa-navicon">Mobile Nav</button>
		<button id="mobile-book" class="transform-button">Book Now</button>
	
		<?php include('inc/i-reservations.php'); ?>

		<?php include('i-nav.php'); ?>
	
		<div class="page-wrapper">	
			<header>
				<div class="sw">
					
					<a href="#" class="logo">
						<img src="../assets/bin/images/hotels/hotel-gander-white-color.svg" alt="Hotel Gander">
					</a>
					
				</div><!-- .sw -->
			</header>
