//load all required scripts
requirejs(
	[
		'scripts/components/placeholder',
		'scripts/components/anchors.external.popup',
		'scripts/components/standard.accordion',
		'scripts/components/gmap',
		'scripts/components/custom.select',
		'scripts/swipe.srf',
		
		'scripts/components/magnific.popup',
		
		'scripts/blocks',
		'scripts/search',
		'scripts/reservation.form',
		'scripts/directions.activities',

		'scripts/nav',
		'scripts/hero'
	],
	function() {		
	
		//IE8 doesn't support SVGs
		if(!Modernizr.svg) {
			$('img').each(function() {
				var src = this.src;
				if(src.indexOf('.svg') !== -1) {
					this.src = src.replace(/\.svg$/,'.png');
				}
			});
		}
		
		//event map
		$('#view-map')
			.on('click',function(e) {
				$('div.event-map').each(function() {
					var
						el = $(this),
						map = el.children('div.map').data('map')
						
					el.toggleClass('visible');
					google.maps.event.trigger(map.map,'resize');
					map.map.setCenter(map.mapOptions.center);
				});
			});
		
});