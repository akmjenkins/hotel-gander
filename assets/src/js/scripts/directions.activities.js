define([
],function() {
		//hotel directions
		(function() {
			
			var methods = {
			
				streetMap: null,
			
				getStreetMap: function() {
					if(!this.streetMap) {
						this.streetMap = $('#street-map');
					}
					
					return this.streetMap;
				},
			
				buildIFrame: function(origin) {
				
					var source = "https://www.google.com/maps/embed/v1/directions?mode=driving";
					source+='&key='+GOOGLE_MAPS_API_KEY;
					source+='&destination='+this.getStreetMap().data('destination');				
					source+='&origin='+origin;
					this.getStreetMap().html('<iframe src="'+source+'" frameborder="0" style="border:0"/>');
				
				},
				
				geolocate: function() {
					var self = this;
					this.getStreetMap().addClass('loading');
					navigator.geolocation.getCurrentPosition(
						function(position) {
							self.onGeolocateDone(position);
						},
						function(positionError) {
							self.onGeolocateFailed(positionError);
						}
					);
				},
				
				onGeolocateDone: function(position) {
					this.buildIFrame(position.coords.latitude+','+position.coords.longitude);
				},
				
				onGelocateFailed: function(positionError) {
					alert(positionError.message);
					this.getStreetMap().removeClass('loading');								
				}
			
			};
			
			$(document)
				.on('submit','#street-form',function() {
					methods.buildIFrame($(this).find('input').val());
					return false;
				})
				.on('click','#directions-geolocate',function() {
					methods.geolocate();
				});
			
		}());
		
		//hotel activities map
		(function() {
		
			var methods = {
			
				activitiesMap: null,
			
				buildIFrame: function(q) {
					var source = "https://www.google.com/maps/embed/v1/search?";
					source+='&key='+GOOGLE_MAPS_API_KEY;
					source+='&center='+this.getActivitiesMap().data('center');
					source+='&zoom='+this.getActivitiesMap().data('zoom');
					source+='&q='+q;
					this.getActivitiesMap().html('<iframe src="'+source+'" frameborder="0" style="border:0"/>');
				},
			
				getActivitiesMap: function() {
					if(!this.activitiesMap) {
						this.activitiesMap = $('#activities-map');
					}
					
					return this.activitiesMap;
				},
				
			};
			
			$(document)
				.on('submit','#activities-form',function() {
					methods.buildIFrame($(this).find('input').val());
					return false;
				});
			
		
		}());

		
	return {};
});