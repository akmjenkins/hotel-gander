var fs = require('fs');
var sizeOf = require('image-size');

var 
	baseDir = 'src/images/sprites/',
	regularDir = baseDir+'regular/',
	twoXDir = baseDir+'@2x/',
	threeXDir = baseDir+'@3x/',
	i=0;

fs.readdir(regularDir,function(err,files) {
	files.forEach(function(name,index) {
		if(name.match('png')) {
			var 
				regularSize = sizeOf(regularDir+name),
				twoXSize = sizeOf(twoXDir+name),
				threeXSize = sizeOf(threeXDir+name);
				
			if(
				(regularSize.width == twoXSize.width/2 && regularSize.height === twoXSize.height/2) &&
				(regularSize.width == threeXSize.width/3 && regularSize.height === threeXSize.height/3)
			) {
				
			} else {
				console.log(name);
			}
			
			
		}
		
	});
	
	console.log(i);
	console.log(files.length);
});